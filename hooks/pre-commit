#!/bin/bash -e

# ############################## #
# ESLint check:
# ############################## #

STAGED_FILES=$(git diff --staged --cached --name-only --diff-filter=ACM)
STAGED_JS_FILES=$((git diff --staged --cached --name-only --diff-filter=ACM | grep ".(j|t)s$" -P) || echo "")
ESLINT="$(git rev-parse --show-toplevel)/node_modules/.bin/eslint"

if [[ "$STAGED_FILES" = "" ]]; then
  echo "No files"
  exit 0
fi

if [[ "$STAGED_JS_FILES" = "" ]]; then
  echo "No changed Javascript or Typescript files to validate"
  exit 0
fi

PASS=true

printf "\nValidating Javascript:\n"

# Check for eslint
if [[ ! -x "$ESLINT" ]]; then
  printf "\t\033[41mPlease install ESlint\033[0m (yarn add eslint -D)"
  exit 1
fi

for FILE in $STAGED_JS_FILES
do
  "$ESLINT" "$FILE" --fix

  if [[ "$?" == 0 ]]; then
    printf "\t\033[32mESLint Passed: $FILE\033[0m\n"
  else
    printf "\t\033[41mESLint Failed: $FILE\033[0m\n"
    PASS=false
  fi
done

printf "\nJavascript validation completed!\n"

if ! $PASS; then
  printf "\033[41mCOMMIT FAILED:\033[0m Your commit contains files that should pass ESLint but do not. Please fix the ESLint errors and try again.\n"
  exit 1
else
  printf "\033[42mESLINT SUCCEEDED\033[0m\n"
fi

# ############################## #
# Prettier check:
# ############################## #

PRETTIER="$(git rev-parse --show-toplevel)/node_modules/.bin/prettier"
# Check for eslint
if [[ ! -x "$PRETTIER" ]]; then
  printf "\t\033[41mPlease install Prettier\033[0m (yarn add prettier -D)"
  exit 1
fi

FILES=$(git diff --cached --name-only --diff-filter=ACM "*.ts" | sed 's| |\\ |g')
[ -z "$FILES" ] && exit 0

# Prettier check all selected files
echo "$FILES" | xargs ./node_modules/.bin/prettier --check

# Add back the modified/prettified files to staging
echo "$FILES" | xargs git add

exit $?
