import { Type } from "class-transformer";
import { IsDateString, IsInt, ValidateNested } from "class-validator";
import { User } from "../users/user";
import { Message } from "./message";

export class Conversation {
  @IsInt()
  id: number;

  @ValidateNested({ each: true })
  @Type(() => Message)
  messages: Message[];

  @ValidateNested({ each: true })
  @Type(() => User)
  users: User[];

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}
