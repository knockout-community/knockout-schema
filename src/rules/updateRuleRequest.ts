import { IsInt, IsOptional, IsPositive, Length, Min } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class UpdateRuleRequest {
  @Length(3, 140)
  @IsOptional()
  @JSONSchema({ description: 'The category of the rule.' })
  category?: string;

  @IsOptional()
  @Length(3, 140)
  @JSONSchema({ description: 'The title of the rule.' })
  title?: string;

  @IsOptional()
  @IsInt()
  @Min(0)
  @JSONSchema({ description: 'The cardinality of the rule relative to other rules.' })
  cardinality?: number;

  @IsOptional()
  @Length(10, 1000)
  @JSONSchema({ description: 'The description of the rule.' })
  description?: string;
}
