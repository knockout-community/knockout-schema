import { IsDateString, IsIn, IsInt, IsOptional, IsPositive, IsString, Min } from 'class-validator';

export class Rule {
  @IsInt()
  id: number;

  @IsOptional()
  @IsString()
  @IsIn(['Subforum', 'Thread'])
  rulableType?: 'Subforum' | 'Thread';

  @IsOptional()
  @IsInt()
  rulableId?: number;

  @IsString()
  category: string;

  @IsString()
  title: string;

  @IsInt()
  @Min(0)
  cardinality: number;

  @IsString()
  description: string;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}
