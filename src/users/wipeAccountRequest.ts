import { MinLength } from 'class-validator';

export class WipeAccountRequest {
  @MinLength(5)
  reason: string;
}
