import { IsIn, IsOptional, IsString } from 'class-validator';

export class UserProfileBackground {
  @IsOptional()
  @IsString()
  url?: string;

  @IsOptional()
  @IsIn(['cover', 'tiled'])
  type?: 'cover' | 'tiled';
}
