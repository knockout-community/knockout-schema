import { IsBoolean, IsOptional, IsString, MaxLength } from 'class-validator';

export class UpdateUserRequest {
  @IsOptional()
  @IsString()
  username?: string;

  @IsOptional()
  @IsString()
  avatarUrl?: string;

  @IsOptional()
  @IsString()
  backgroundUrl?: string;

  @IsOptional()
  @IsString()
  @MaxLength(25)
  pronouns?: string;

  @IsOptional()
  @IsBoolean()
  showOnlineStatus?: boolean;

  @IsOptional()
  @IsBoolean()
  disableIncomingMessages?: boolean;
}
