import { Type } from 'class-transformer';
import {
  IsOptional,
  IsString,
  IsUrl,
  MaxLength,
  ValidateIf,
  ValidateNested,
} from 'class-validator';
import { SteamData } from './steamData';

export class UserProfileSocial {
  @IsOptional()
  @ValidateIf((e) => e.website !== '')
  @IsUrl({ require_protocol: true })
  website?: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => SteamData)
  steam?: SteamData;

  @IsOptional()
  @IsString()
  @MaxLength(37)
  discord?: string;

  @IsOptional()
  @MaxLength(35)
  github?: string;

  @IsOptional()
  @ValidateIf((e) => e.youtube !== '')
  @IsUrl()
  youtube?: string;

  @IsOptional()
  @IsString()
  @MaxLength(35)
  twitter?: string;

  @IsOptional()
  @IsString()
  fediverse?: string;

  @IsOptional()
  @IsString()
  @MaxLength(35)
  twitch?: string;

  @IsOptional()
  @IsString()
  @MaxLength(35)
  gitlab?: string;

  @IsOptional()
  @IsString()
  @MaxLength(35)
  tumblr?: string;

  @IsOptional()
  @IsString()
  @MaxLength(45)
  bluesky?: string;
}
