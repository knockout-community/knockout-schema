import { Type } from 'class-transformer';
import { IsBoolean, IsOptional, MaxLength, ValidateNested } from 'class-validator';
import { UserProfileSocial } from './userProfileSocial';

export class UpdateUserProfileRequest {
  @MaxLength(160)
  bio: string;

  @ValidateNested()
  @Type(() => UserProfileSocial)
  social: UserProfileSocial;

  @IsBoolean()
  disableComments: boolean;

  @IsOptional()
  @IsBoolean()
  useBioForTitle?: boolean;
}
