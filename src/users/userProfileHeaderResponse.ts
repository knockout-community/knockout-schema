import { IsString } from 'class-validator';

export class UserProfileHeaderResponse {
  @IsString()
  header: string;
}
