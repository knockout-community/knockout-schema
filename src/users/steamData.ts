import { IsOptional, IsString, IsUrl, MaxLength, ValidateIf } from 'class-validator';
import { USERNAME_CHARACTER_LIMIT } from '../limits';

export class SteamData {
  @IsOptional()
  @MaxLength(USERNAME_CHARACTER_LIMIT)
  name?: string;

  @IsOptional()
  @ValidateIf((e) => e.url !== '')
  @IsUrl({ require_protocol: true, host_whitelist: ['steamcommunity.com'] })
  url: string;
}
