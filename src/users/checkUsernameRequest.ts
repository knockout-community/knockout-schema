import { IsOptional, IsString } from 'class-validator';

export class CheckUsernameRequest {
  @IsOptional()
  @IsString()
  username: string;
}
