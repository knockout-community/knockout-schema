import { MaxLength } from 'class-validator';
import { PROFILE_COMMENT_CHARACTER_LIMIT } from '../limits';

export class CreateUserProfileCommentRequest {
  @MaxLength(PROFILE_COMMENT_CHARACTER_LIMIT)
  content: string;
}
