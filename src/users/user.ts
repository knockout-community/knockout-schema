import { IsBoolean, IsDateString, IsInt, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { Role } from '../roles/role';
import { UserProfile } from './userProfile';

export class User {
  @IsInt()
  id: number;

  @ValidateNested()
  role: Role;

  @IsString()
  username: string;

  @IsInt()
  usergroup: number;

  @IsString()
  avatarUrl: string;

  @IsString()
  backgroundUrl: string;

  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  pronouns?: string;

  @IsInt()
  posts: number;

  @IsInt()
  threads: number;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;

  @IsBoolean()
  banned: boolean;

  stripeCustomerId?: string;

  donationUpgradeExpiresAt?: string;

  @IsBoolean()
  online?: boolean;

  disableIncomingMessages?: boolean;

  @IsOptional()
  @Type(() => UserProfile)
  profile?: UserProfile;
}
