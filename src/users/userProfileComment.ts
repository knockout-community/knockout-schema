import { IsBoolean, IsDateString, IsInt, IsObject, IsString } from 'class-validator';
import { User } from './user';

export class UserProfileComment {
  @IsInt()
  id: number;

  @IsInt()
  userProfile: number;

  @IsObject()
  author: User;

  @IsString()
  content: string;

  @IsBoolean()
  deleted: boolean;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}
