import { IsDateString, IsString } from "class-validator";

export class PreviousUsername {
  @IsString()
  username: string;

  @IsDateString()
  createdAt: string;
}