import { PollSettings } from '../polls/pollSettings';
import { Type } from 'class-transformer';

export class UpdatePollRequest {
  @Type(() => PollSettings)
  poll_settings: PollSettings;
}
