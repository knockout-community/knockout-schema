import { IsDateString, IsNumber, IsString, MaxLength, MinLength } from "class-validator";

export class PollOption {
  @IsNumber()
  id: number;

  @IsNumber()
  pollId: number;

  @IsString()
  description: string;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}