import { Type } from "class-transformer";
import { IsArray, IsBoolean, IsDateString, IsEnum, IsNumber, IsOptional, IsString, ValidateNested } from "class-validator";
import { PollOption } from "./pollOption";
import { PollType } from "./pollType";
import { PollResult } from "./pollResult";

export class Poll {
  @IsNumber()
  id: number;

  @IsNumber()
  postId: number;

  @IsString()
  title: string;

  @IsEnum(PollType)
  type: PollType;

  @IsBoolean()
  anonymous: boolean;

  @IsOptional()
  @IsDateString()
  expiresAt?: string;

  @IsBoolean()
  hideResponsesUntilExpired: boolean;

  @IsArray()
  @Type(() => PollOption)
  options: PollOption[];

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => PollResult)
  results: PollResult[];

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}