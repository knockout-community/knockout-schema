import { Type } from "class-transformer";
import { IsArray, IsNumber, ValidateNested } from "class-validator";
import { User } from "../users/user";

export class PollResult {
  @IsNumber()
  pollOptionId: number;

  @IsNumber()
  count: number;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => User)
  sampleUsers: User[];
}