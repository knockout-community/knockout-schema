import { IsOptional, IsString, IsEnum, ValidateIf, IsBoolean, IsArray, ArrayMinSize, ArrayMaxSize, MinLength, MaxLength, IsDateString } from "class-validator";
import { PollType } from "./pollType";

export class PollSettings {
  @IsString()
  title?: string;

  @IsEnum(PollType)
  type?: PollType;

  @IsOptional()
  @IsBoolean()
  anonymous?: boolean;

  @IsOptional()
  @IsDateString()
  expiresAt?: string;

  @IsOptional()
  @IsBoolean()
  hideResponsesUntilExpired?: boolean;
  
  @IsArray()
  @ArrayMinSize(1)
  @ArrayMaxSize(10)
  @IsString({ each: true })
  @MinLength(1, { each: true })
  @MaxLength(255, { each: true })
  options?: string[];
}