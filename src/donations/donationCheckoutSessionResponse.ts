import { IsString } from 'class-validator';

export class DonationCheckoutSessionResponse {
  @IsString()
  sessionId: string;
}
