import { IsString } from 'class-validator';

export class DonationSyncRequest {
  @IsString()
  sessionId: string;
}
