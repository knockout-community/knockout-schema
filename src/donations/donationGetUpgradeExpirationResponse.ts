import { IsDateString } from 'class-validator';

export class DonationGetUpgradeExpirationResponse {
  @IsDateString()
  expiresAt?: string;
}
