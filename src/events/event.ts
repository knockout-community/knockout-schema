import { Type } from 'class-transformer';
import { IsDateString, IsEnum, IsInt, IsObject, IsOptional, ValidateNested } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';
import { Ban } from '../bans/ban';
import { User } from '../users/user';
import { CalendarEvent } from '../calendarEvents/calendarEvent';
import ThreadWithSubforum from '../threads/threadWithSubforum';
import { Post } from '../posts/post';

export enum EventType {
  THREAD_CREATED = 'thread-created',
  THREAD_LOCKED = 'thread-locked',
  THREAD_UNLOCKED = 'thread-unlocked',
  THREAD_DELETED = 'thread-deleted',
  THREAD_RESTORED = 'thread-restored',
  THREAD_MOVED = 'thread-moved',
  THREAD_PINNED = 'thread-pinned',
  THREAD_UNPINNED = 'thread-unpinned',
  THREAD_RENAMED = 'thread-renamed',
  THREAD_POST_LIMIT_REACHED = 'thread-post-limit-reached',
  THREAD_BACKGROUND_UPDATED = 'thread-background-updated',
  BAN_REASON_EDITED = 'ban-reason-edited',
  USER_BANNED = 'user-banned',
  USER_UNBANNED = 'user-unbanned',
  USER_WIPED = 'user-wiped',
  USER_AVATAR_REMOVED = 'user-avatar-removed',
  USER_BACKGROUND_REMOVED = 'user-background-removed',
  USER_PROFILE_REMOVED = 'user-profile-removed',
  GOLD_EARNED = 'gold-earned',
  GOLD_LOST = 'gold-lost',
  POST_CREATED = 'post-created',
  RATING_CREATED = 'rating-created',
  PROFILE_COMMENT_CREATED = 'profile-comment-created',
  CALENDAR_EVENT_CREATED = 'calendar-event-created',
}

export const THREAD_EVENT_TYPES = [
  EventType.THREAD_CREATED,
  EventType.THREAD_BACKGROUND_UPDATED,
  EventType.THREAD_DELETED,
  EventType.THREAD_MOVED,
  EventType.THREAD_PINNED,
  EventType.THREAD_UNPINNED,
  EventType.THREAD_LOCKED,
  EventType.THREAD_UNLOCKED,
  EventType.THREAD_RENAMED,
  EventType.THREAD_RESTORED,
  EventType.THREAD_POST_LIMIT_REACHED,
];

export const BAN_EVENT_TYPES = [
  EventType.USER_BANNED,
  EventType.USER_WIPED,
  EventType.BAN_REASON_EDITED,
];

export const CALENDAR_EVENT_EVENT_TYPES = [EventType.CALENDAR_EVENT_CREATED];

export class Event {
  @IsInt()
  id: number;

  @IsEnum(EventType)
  type: EventType;

  @ValidateNested()
  @Type(() => ThreadWithSubforum)
  @JSONSchema({
    oneOf: [
      { $ref: '#/components/schemas/ThreadWithSubforum' },
      { $ref: '#/components/schemas/User' },
      { $ref: '#/components/schemas/Ban' },
      { $ref: '#/components/schemas/CalendarEvent' },
      { $ref: '#/components/schemas/Post' },
    ],
  })
  data: ThreadWithSubforum | User | Ban | CalendarEvent | Post;

  @ValidateNested()
  creator: User;

  @IsObject()
  @IsOptional()
  content: Record<string, string>;

  @IsDateString()
  createdAt: string;
}

export interface ThreadEvent extends Event {
  data: ThreadWithSubforum;
  type:
    | EventType.THREAD_CREATED
    | EventType.THREAD_BACKGROUND_UPDATED
    | EventType.THREAD_DELETED
    | EventType.THREAD_MOVED
    | EventType.THREAD_PINNED
    | EventType.THREAD_UNPINNED
    | EventType.THREAD_LOCKED
    | EventType.THREAD_UNLOCKED
    | EventType.THREAD_RENAMED
    | EventType.THREAD_RESTORED
    | EventType.THREAD_POST_LIMIT_REACHED;
}

export interface PostEvent extends Event {
  data: Post;
  type: EventType.POST_CREATED | EventType.RATING_CREATED;
}

export interface BanEvent extends Event {
  data: Ban;
  type: EventType.USER_BANNED | EventType.USER_WIPED | EventType.BAN_REASON_EDITED;
}

export interface CalendarEventEvent extends Event {
  data: CalendarEvent;
  type: EventType.CALENDAR_EVENT_CREATED;
}

export interface UserEvent extends Event {
  data: User;
  type:
    | EventType.USER_UNBANNED
    | EventType.USER_AVATAR_REMOVED
    | EventType.USER_BACKGROUND_REMOVED
    | EventType.USER_PROFILE_REMOVED
    | EventType.PROFILE_COMMENT_CREATED
    | EventType.GOLD_EARNED
    | EventType.GOLD_LOST;
}
