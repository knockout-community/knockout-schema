import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsDateString,
  IsISO31661Alpha2,
  IsNumber,
  IsObject,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Poll } from '../polls/poll';
import { User } from '../users/user';
import { Rating } from './rating';
import { Thread } from '../threads/thread';

export class Post {
  @IsNumber()
  id: number;

  @IsNumber()
  threadId: number;

  @IsOptional()
  @ValidateNested()
  thread?: Thread;

  @IsNumber()
  page: number;

  @IsString()
  content: string;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;

  @IsNumber()
  userId: number;

  @IsOptional()
  @ValidateNested()
  user?: User;

  @ValidateNested({ each: true })
  @Type(() => Rating)
  ratings: Rating[];

  @IsObject({ each: true })
  bans: object[];

  @IsNumber()
  threadPostNumber: number;

  @IsOptional()
  @IsString()
  countryName?: string;

  @IsOptional()
  @IsISO31661Alpha2()
  countryCode?: string;

  @IsOptional()
  @IsString()
  appName?: string;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => User)
  mentionUsers?: User[];

  @IsOptional()
  @Type(() => Poll)
  poll?: Poll

  @IsBoolean()
  pinned?: boolean;

  @IsOptional()
  @IsBoolean()
  distinguished?: boolean;
}
