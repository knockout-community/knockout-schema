import { IsBoolean, IsNumber, IsOptional } from 'class-validator';
import { UpdatePostRequest } from './updatePostRequest';
import { PollSettings } from '../polls/pollSettings';
import { Type } from 'class-transformer';

export class CreatePostRequest extends UpdatePostRequest {
  @IsNumber()
  thread_id: number;

  @IsOptional()
  @IsBoolean()
  display_country?: boolean;
 
  @IsOptional()
  @IsBoolean()
  pinned?: boolean;

  @IsOptional()
  @Type(() => PollSettings)
  poll_settings?: PollSettings;
}
