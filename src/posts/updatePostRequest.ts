import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class UpdatePostRequest {
  @IsNotEmpty()
  content: string;

  @IsOptional()
  @IsString()
  app_name?: string;

  @IsOptional()
  @IsString()
  editReason?: string;
  
  @IsOptional()
  @IsBoolean()
  pinned?: boolean;

  @IsOptional()
  @IsBoolean()
  distinguished?: boolean;
}
