import { IsEnum } from 'class-validator';
import { RatingValue } from './rating';

export class RatePostRequest {
  @IsEnum(RatingValue)
  rating: RatingValue;
}
