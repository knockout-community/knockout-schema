import { Type } from 'class-transformer';
import { IsEnum, IsNumber, ValidateNested } from 'class-validator';
import { User } from '../users/user';

export enum RatingValue {
  AGREE = 'agree',
  DISAGREE = 'disagree',
  FUNNY = 'funny',
  FRIENDLY = 'friendly',
  KAWAII = 'kawaii',
  SAD = 'sad',
  ARTISTIC = 'artistic',
  INFORMATIVE = 'informative',
  IDEA = 'idea',
  WINNER = 'winner',
  GLASSES = 'glasses',
  LATE = 'late',
  CITATION = 'citation',
  OPTIMISTIC = 'optimistic',
  ZING = 'zing',
  YEET = 'yeet',
  RUDE = 'rude',
  CONFUSING = 'confusing',
  SCARY = 'scary',
}

export class Rating {
  @IsNumber()
  id: number;

  @IsEnum(RatingValue)
  rating: RatingValue;

  @IsNumber()
  ratingId: number;

  @ValidateNested({ each: true })
  @Type(() => User)
  users: User[];

  @IsNumber()
  count: number;
}
