import {
  IsOptional,
  ValidateNested,
} from 'class-validator';
import { Post } from './post';
import { Type } from 'class-transformer';

export class PostWithResponses extends Post {
  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => Post)
  responses?: Post[];
}
