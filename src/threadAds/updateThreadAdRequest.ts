import { IsOptional, IsString, Length } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class UpdateThreadAdRequest {
  @IsString()
  @Length(5, 150)
  @IsOptional()
  @JSONSchema({ description: 'The description of the thread for this Thread Ad' })
  description?: string;

  @IsString()
  @Length(5, 200)
  @IsOptional()
  @JSONSchema({
    description:
      'The search query this user will be directed to when the user clicks this Thread Ad',
  })
  query?: string;

  @IsString()
  @IsOptional()
  @JSONSchema({ description: 'The filename of this thread ad image' })
  filename?: string;
}