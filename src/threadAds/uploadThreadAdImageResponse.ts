import { IsString } from "class-validator";

export class UploadThreadAdImageResponse {
  @IsString()
  filename: string;
}