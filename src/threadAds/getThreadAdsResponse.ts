import { Type } from 'class-transformer';
import { IsInt, ValidateNested } from 'class-validator';
import { ThreadAd } from './threadAd';

export class GetThreadAdsResponse {
  @ValidateNested({ each: true })
  @Type(() => ThreadAd)
  threadAds: ThreadAd[];

  @IsInt()
  count: number;
}