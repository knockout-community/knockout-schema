import { Type } from 'class-transformer';
import { IsArray, IsInt } from 'class-validator';
import { ThreadSearchResult } from './threadSearchResult';

export class ThreadSearchResponse {
  @IsInt()
  totalThreads: number;

  @IsArray()
  @Type(() => ThreadSearchResult)
  threads: ThreadSearchResult[];
}
