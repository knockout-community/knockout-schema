import { ArrayMaxSize, IsBoolean, IsIn, IsInt, IsOptional, IsPositive, Length } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class ThreadSearchRequest {
  @Length(5, 140)
  @JSONSchema({ description: 'Required term that filters against thread titles' })
  title: string;

  @IsOptional()
  @ArrayMaxSize(3)
  @IsInt({ each: true })
  @JSONSchema({
    description:
      'Term that filters against thread tag IDs. If null, threads with zero or any tags will be searched.',
  })
  tag_ids?: number[];

  @IsOptional()
  @IsInt()
  @JSONSchema({
    description:
      'Term that filters against thread Subforum IDs. If null, threads in all subforums will be searched.',
  })
  subforum_id?: number;

  @IsOptional()
  @IsInt()
  @JSONSchema({
    description:
      'Term that filters against thread creator User IDs. If null, threads with any creator will be searched.',
  })
  user_id?: number;

  @IsOptional()
  @IsIn(['created_at', 'updated_at', 'relevance'])
  @JSONSchema({
    description: 'The sort criteria for the results. Defaults to relevance.',
  })
  sort_by?: 'created_at' | 'updated_at' | 'relevance';

  @IsOptional()
  @IsIn(['desc', 'asc'])
  @JSONSchema({
    description:
      'The sort order for the results. Defaults to newest or highest first, depending on the sort_by parameter. Relevance can only be sorted in descending order.',
  })
  sort_order?: 'desc' | 'asc';

  @IsOptional()
  @IsBoolean()
  @JSONSchema(({
    description: 'Whether to include locked threads in the search results.'
  }))
  locked?: boolean;
  
  @IsOptional()
  @IsInt()
  @IsPositive()
  @JSONSchema({
    description: 'The page of the results. There are 10 results per page. Defaults to 1.',
  })
  page?: number;
}
