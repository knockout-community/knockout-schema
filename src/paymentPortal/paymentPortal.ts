import { IsString } from 'class-validator';

export class PaymentPortal {
  @IsString()
  url: string;
}
