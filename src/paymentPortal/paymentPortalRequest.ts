import { IsString } from 'class-validator';

export class PaymentPortalRequest {
  @IsString()
  returnUrl: string;
}
