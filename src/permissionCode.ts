export enum ResourceAction {
  CREATE = 'create',
  VIEW = 'view',
  UPDATE = 'update',
  ARCHIVE = 'archive',
}

export enum Resource {
  BAN = 'ban',
  RULE = 'rule',
  EVENT = 'event',
  IP_ADDRESS = 'ipAddress',
  MESSAGE = 'message',
  MESSAGE_OF_THE_DAY = 'messageOfTheDay',
  RATING = 'rating',
  REPORT = 'report',
  ROLE = 'role',
  TAG = 'tag',
  THREAD_AD = 'threadAd',
  THREAD_TAG = 'threadTag',
  USER = 'user',
  SUBFORUM = 'subforum',
  LATEST_USERS = 'latest-users',
  DASHBOARD_DATA = 'dashboard-data',
  FULL_USER_INFO = 'full-user-info',
  GOLD_PRODUCT = 'gold-product',
  ADMIN_SETTINGS = 'admin-settings',
  USER_PROFILE_COMMENT = 'user-profile-comment',
  CALENDAR_EVENT = 'calendar-event',
}

type Permission =
  | `${Resource}-${ResourceAction}`
  | 'user-avatar-gif-upload'
  | 'report-resolve'
  | 'message-create-override';

// these are codes that will be created along with all existing subforums
// e.g. the subforum with the id 1 will be associated with
// the permission codes 'subforum-1-view', 'subforum-1-update', etc
export enum SubforumPermissionSuffixes {
  // view all the threads in the the subforum (applies to home page and post visibility as well)
  VIEW = 'view',
  // view only the users own threads,
  VIEW_OWN_THREADS = 'view-own-threads',
  // update the subforum name or description
  UPDATE = 'update',
  // archive the subforum
  ARCHIVE = 'archive',
  // create a post
  POST_CREATE = 'post-create',
  // update a post
  POST_UPDATE = 'post-update',
  // archive a post
  POST_ARCHIVE = 'post-archive',
  // bypass post validations
  POST_BYPASS_VALIDATIONS = 'post-bypass-validations',
  // rate a post
  POST_RATING_CREATE = 'post-rating-create',
  // report a post
  POST_REPORT_CREATE = 'post-report-create',
  // create a thread
  THREAD_CREATE = 'thread-create',
  // update a thread title
  THREAD_UPDATE = 'thread-update',
  // lock a thread
  THREAD_LOCK = 'thread-lock',
  // unlock a thread
  THREAD_UNLOCK = 'thread-unlock',
  // pin a thread
  THREAD_PIN = 'thread-pin',
  // unpin a thread
  THREAD_UNPIN = 'thread-unpin',
  // change the subforum of a thread
  THREAD_MOVE = 'thread-move',
  // delete a thread
  THREAD_DELETE = 'thread-delete',
  // view deleted threads
  VIEW_DELETED_THREADS = 'view-deleted-threads',
  // view thread viewers
  VIEW_THREAD_VIEWERS = 'view-thread-viewers',
  // change the background of a thread
  THREAD_BACKGROUND_UPDATE = 'thread-background-update',
  // create a rule
  RULE_CREATE = 'rule-create',
  // update a rule
  RULE_UPDATE = 'rule-update',
  // archive a rule
  RULE_ARCHIVE = 'rule-archive',
}

type SubforumPermission = `subforum-${number}-${SubforumPermissionSuffixes}`;

export type PermissionCode = Permission | SubforumPermission;