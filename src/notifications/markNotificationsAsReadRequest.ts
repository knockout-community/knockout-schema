import { IsInt } from 'class-validator';

export class MarkNotificationsAsReadRequest {
  @IsInt({ each: true })
  notificationIds: number[];
}
