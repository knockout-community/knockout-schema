import { Type } from 'class-transformer';
import { IsBoolean, IsDateString, IsEnum, IsInt, ValidateNested } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';
import { Conversation } from '../conversations/conversation';
import { Post } from '../posts/post';

export enum NotificationType {
  POST_REPLY = 'POST_REPLY',
  MESSAGE = 'MESSAGE',
  PROFILE_COMMENT = 'PROFILE_COMMENT',
  REPORT_RESOLUTION = 'REPORT_RESOLUTION',
  POST_MENTION = 'POST_MENTION'
}

export class Notification {
  @IsInt()
  id: number;

  @IsEnum(NotificationType)
  type: NotificationType;

  @ValidateNested()
  @Type(() => Conversation)
  @JSONSchema({
    oneOf: [{ $ref: '#/components/schemas/Post' }, { $ref: '#/components/schemas/Conversation' }],
  })
  data: Post | Conversation;

  @IsInt()
  userId: number;

  @IsBoolean()
  read: boolean;

  @IsDateString()
  createdAt: string;
}
