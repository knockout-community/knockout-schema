import { IsString } from 'class-validator';

export class GoldSubscriptionCheckoutResponse {
  @IsString()
  sessionId: string;
}
