import { IsString } from 'class-validator';

export class GoldSubscriptionCheckoutRequest {
  @IsString()
  successUrl: string;

  @IsString()
  cancelUrl: string;
}
