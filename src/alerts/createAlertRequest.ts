import { IsDateString, IsInt, IsOptional } from 'class-validator';

export class CreateAlertRequest {
  @IsInt()
  threadId: number;

  lastSeen?: string;

  @IsInt()
  lastPostNumber: number;
}
