import { Type } from 'class-transformer';
import { IsArray, IsInt, ValidateNested } from 'class-validator';
import { Alert } from './alert';

export class AlertsResponse {
  @ValidateNested({ each: true })
  @Type(() => Alert)
  alerts: Alert[];

  @IsInt()
  totalAlerts: number;

  @IsArray()
  ids: number[];
}
