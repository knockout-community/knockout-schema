import { IsInt, ValidateNested } from 'class-validator';
import { ThreadWithLastPost } from '../threads/threadWithLastPost';

export class Alert {
  @IsInt()
  id: number;

  @ValidateNested()
  thread: ThreadWithLastPost;

  @IsInt()
  unreadPosts: number;

  @IsInt()
  firstUnreadId: number;
}
