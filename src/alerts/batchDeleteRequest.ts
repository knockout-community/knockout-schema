import { IsArray } from 'class-validator';

export class BatchDeleteRequest {
  @IsArray()
  threadIds: number[];
}
