import { IsInt, IsISO8601, IsNotEmpty, IsPositive, IsString, Length } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class CreateCalendarEventRequest {
  @Length(3, 255)
  @IsNotEmpty()
  @IsString()
  @JSONSchema({ description: 'The title of the Calendar Event.' })
  title: string;

  @Length(3, 1000)
  @IsNotEmpty()
  @IsString()
  @JSONSchema({ description: 'The description of the Calendar Event.' })
  description: string;

  @IsInt()
  @IsPositive()
  @JSONSchema({ description: 'The Thread ID of the Calendar Event.' })
  threadId: number;

  @IsISO8601()
  @IsNotEmpty()
  @JSONSchema({ description: 'The start date of the Calendar Event in ISO 8601 format.' })
  startsAt: string;

  @IsISO8601()
  @IsNotEmpty()
  @JSONSchema({ description: 'The end date of the Calendar Event in ISO 8601 format.' })
  endsAt: string;
}
