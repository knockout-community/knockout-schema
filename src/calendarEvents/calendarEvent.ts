import { Type } from 'class-transformer';
import { IsDateString, IsInt, IsString } from 'class-validator';
import { Thread } from '../threads/thread';
import { User } from '../users/user';

export class CalendarEvent {
  @IsInt()
  id: number;

  @IsString()
  title: string;

  @IsString()
  description: string;

  @Type(() => Thread)
  thread: Thread;

  @Type(() => User)
  createdBy: User;

  @IsDateString()
  startsAt: string;

  @IsDateString()
  endsAt: string;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}
