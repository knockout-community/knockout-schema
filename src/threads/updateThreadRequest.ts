import { IsIn, IsInt, IsOptional, IsUrl, Length } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class UpdateThreadRequest {
  @IsOptional()
  @Length(3, 140)
  @JSONSchema({ description: 'The title of the thread.' })
  title?: string;

  @IsOptional()
  @IsUrl()
  @JSONSchema({ description: "The URL of the thread's background." })
  background_url?: string;

  @IsOptional()
  @IsIn(['cover', 'tiled'])
  @JSONSchema({ description: 'Whether the background should cover the page or be tiled.' })
  background_type?: 'cover' | 'tiled';

  @IsOptional()
  @IsInt()
  @JSONSchema({ description: "The id of the thread's subforum." })
  subforum_id?: number;
}
