import { IsInt } from 'class-validator';
import { Thread } from './thread';

export class NewThread extends Thread {
  @IsInt()
  userId: number;
}
