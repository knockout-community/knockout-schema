import { ArrayMaxSize, IsInt } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class UpdateThreadTagsRequest {
  @ArrayMaxSize(3)
  @IsInt({ each: true })
  @JSONSchema({ description: "A list of ids for the thread's tags." })
  tag_ids: number[];
}
