import { ValidateNested } from 'class-validator';
import { Thread } from './thread';
import { Subforum } from '../subforums/subforum';

export default class ThreadWithSubforum extends Thread {
  @ValidateNested()
  subforum: Subforum;
}
