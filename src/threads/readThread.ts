import { IsBoolean, IsInt } from 'class-validator';

export class ReadThread {
  @IsInt()
  firstUnreadId: number;

  @IsBoolean()
  isSubscription: boolean;

  @IsInt()
  lastPostNumber: number;

  lastSeen?: string;

  @IsInt()
  unreadPostCount: number;
}
