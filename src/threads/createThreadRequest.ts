import { ArrayMaxSize, IsIn, IsInt, IsOptional, IsUrl, Length, ValidateIf } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';
import { POST_CHARACTER_LIMIT } from '../limits';
import { Type } from 'class-transformer';
import { PollSettings } from '../polls/pollSettings';

export class CreateThreadRequest {
  @Length(3, 140)
  @JSONSchema({ description: 'The title of the thread.' })
  title: string;

  @Length(1, POST_CHARACTER_LIMIT)
  @JSONSchema({ description: 'The content of the first post in the thread.' })
  content: string;

  @IsInt()
  @JSONSchema({ description: "An id corresponding to the thread's icon." })
  icon_id: number;

  @IsInt()
  @JSONSchema({ description: "The id of the thread's subforum." })
  subforum_id: number;

  @IsOptional()
  @ArrayMaxSize(3)
  @IsInt({ each: true })
  @JSONSchema({ description: "A list of ids for the thread's tags." })
  tag_ids?: number[];

  @ValidateIf((obj) => obj.background_type !== undefined)
  @IsUrl()
  @JSONSchema({ description: "The URL of the thread's background." })
  background_url?: string;

  @ValidateIf((obj) => obj.background_url !== undefined)
  @IsIn(['cover', 'tiled'])
  @JSONSchema({ description: 'Whether the background should cover the page or be tiled.' })
  background_type?: 'cover' | 'tiled';
  
  @IsOptional()
  @Type(() => PollSettings)
  @JSONSchema({ description: 'The settings for the poll associated with the thread.' })
  poll_settings?: PollSettings;
}
