import { Type } from 'class-transformer';
import { IsInt, ValidateNested } from 'class-validator';
import { PostWithResponses } from '../posts/postWithResponses';
import { ThreadWithLastPost } from './threadWithLastPost';
import { Subforum } from '../subforums/subforum';

export class ThreadWithPosts extends ThreadWithLastPost {
  @IsInt()
  currentPage: number;

  @ValidateNested()
  subforum: Subforum;

  @ValidateNested({ each: true })
  @Type(() => PostWithResponses)
  posts: PostWithResponses[];
}
