import { IsDateString, IsInt, IsString } from 'class-validator';

export class ThreadMetadata {
  @IsString()
  title: string;

  @IsInt()
  iconId: number;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;

  @IsString()
  subforumName: string;

  @IsString()
  username: string;
}
