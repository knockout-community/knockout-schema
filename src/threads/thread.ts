import {
  IsBoolean,
  IsDateString,
  IsIn,
  IsInt,
  IsString,
  IsUrl,
  ValidateNested,
} from 'class-validator';
import { User } from '../users/user';

export class Thread {
  @IsInt()
  id: number;

  @IsString()
  title: string;

  @IsInt()
  iconId: number;

  @IsInt()
  subforumId: number;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;

  @IsDateString()
  deletedAt: string | null;

  @IsBoolean()
  locked: boolean;

  @IsBoolean()
  pinned: boolean;

  @ValidateNested()
  user: User;

  @IsUrl()
  backgroundUrl?: string;

  @IsIn(['cover', 'tiled'])
  backgroundType?: 'cover' | 'tiled';
}
