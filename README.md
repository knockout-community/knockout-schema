# Knockout Schema

These are Typescript definitions shared between Knockout's [API](https://gitlab.com/knockout-community/knockout-api/) and [frontend](https://gitlab.com/knockout-community/knockout-front/).

## Developing locally

To sync your local branch of the API or frontend with the latest schema run:

```
yarn up knockout-schema@https://gitlab.com/knockout-community/knockout-schema.git
```

If your work also requires changes to the schema, you can make your repo point to your local copy by running:

```
yarn link <path to knockout-schema repo>
yarn
```

Make sure to revert your changes to `package.json` and `yarn.lock` before you make a pull request.

# Contributing to knockout-schema

1. Make a feature branch targeting main
2. Do work, push it to your branch
3. Create a pull request, linking the corresponding API or frontend pull request
